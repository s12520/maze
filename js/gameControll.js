function Starter() {
    if (game.input.activePointer.isDown) {
        testerOn++;
        pather = 999;
    }
}

function pointGenerator() {
    var i = 0;
    while (i == 0) {
        var pointXtemp = Math.floor(Math.random() * mazeWidth) + 1;
        var pointYtemp = Math.floor(Math.random() * mazeHeight - 1) + 1;

        if (maze[pointYtemp][pointXtemp] == 0) {
            i++;
        }
    }
    return [pointXtemp, pointYtemp];
}